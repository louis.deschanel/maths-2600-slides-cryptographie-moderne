import random


def rsa_keygen() -> dict:
    e = 65537
    # Implementez la génération de clef de RSA avec e = 65537
    # 1. Tire aléatoirement un nombre premier p avec la fonction draw_random_prime
    # 2. Tire aléatoirement un nombre premier q avec la fonction draw_random_prime
    # 3. Calcul de d, l'inverse de e modulo (p - 1) * (q - 1), avec la fonction modular_inverse
    # 4. Renvoit un dictionnaire { "public_key": (e, p * q), "private_key": d}
    pass


def rsa_encrypt(msg: str, public_key: tuple) -> int:
    # Implementez le chiffrement rsa d'un message avec une clef publique de la forme (e, N)
    # 1. Convertir le message en nombre entier avec la fonction str_to_int
    # 2. Verifiez que ce nombre est < public_key[1], sinon lancer une exception
    # 3. Chiffrez le nombre entier avec pow_mod et les paramètre de la clef publique (e, N)
    pass


def rsa_decrypt(msg: int, key: dict) -> str:
    # Implementez le dechiffrement rsa d'un message avec une clef de la forme { "public_key": (e, p * q), "private_key": d}
    # 1. Utilisez pow_mod avec les paramètres de la clef
    # 2. Convertir l'entier calculé en str avec la fonction int_to_str
    pass


def orchestrate_rsa():
    # Implementez une orchestration de RSA
    # 1. Generez et afficher la clef
    # 2. Chiffrez un message et afficher l'entier resultant
    # 3. Dechiffrez l'entier et verifier que le message obtenu est l'original
    pass


the_key = {
    'public_key': (65537, 5160258950707004527433623694972158618124318233517473047143553795491940821509623478999918620803781490217484710013203990920469036477279549580526508159601864388073082079512341058264577952562685541104181983806385199170060477093723804132167472821471700912412626406630441429793392837273636598268817240946497506820181915787509671527530995465765189468225857652273536564402340541728949333875068519127712897006118486068587904480131024032316438716419790315599017159081820735583525204106378011177934323267819214343749323750085324401804369000005522035908418807110114718039261271305886368333912351391303836457593948335395855507583),
    'private_key': 627936358574368083773794176837556875956199366957624663182169484704033264439007694051060484930804848932426576778847091377248585774544217890728884486211222187388541275678027983271434596818399029407904715212107999502284698793390715747654540118577853956947841609974179019524883773704277764792313006340667372886909680395607160112990863561229594521434907523415894476610812782204805233492229679822906086298065600687219804109153860239235505216857720372348199986727139109826857982310524023571110322296236136269506896654238646838784117269757980736109840900745065339439594650937314449703712101022601412396454202821492505234473
}


cypher_text = 602615695538398865986103808888324609946059458992453782893761508370364126434765480745925456008311110097781088480248720248090596169378272945034170486888633924019689244629365827705143735859884159481048823046306550922413690593957216042187301328427358353776920173019807815522432899231960604161765261470093563432833290945028180426672801670399260070325291950555331382320004409327959232953037211763370983500859883074329273915004363416303271902383762493068939220935480452285519833077108260421677790570729682150610949840577977481271582539442140105973442786832608699078905470007784090128822980202327104900727766440040286840010


def bytes_to_byte_strings(bytes_: list) -> list:
    return [f"{b:08b}" for b in bytes_]


def concat_byte_strings(byte_strings: list):
    return "".join(byte_strings)


def str_to_binary(s: str) -> str:
    return concat_byte_strings(bytes_to_byte_strings(s.encode()))


def str_to_int(s: str) -> str:
    return int(str_to_binary(s), 2)


def split_byte_strings(s: str) -> list:
    return [s[i : i + 8] for i in range(0, len(s), 8)]


def byte_strings_to_bytes(bytes_: list) -> list:
    return [int(c, 2) for c in bytes_]


def bytes_to_str(bytes_: list) -> str:
    return bytes(bytes_).decode()


def int_to_binary(value: int) -> str:
    from math import ceil

    s = f"{value:b}"
    size = ceil(len(s) / 8) * 8
    return f"{s:0>{size}}"


def int_to_str(value: int) -> str:
    s = int_to_binary(value)
    return bytes_to_str(byte_strings_to_bytes(split_byte_strings(int_to_binary(value))))


def miller_rabin(n, k):
    # Implementation uses the Miller-Rabin Primality Test
    # The optimal number of rounds for this test is 40
    # See http://stackoverflow.com/questions/6325576/how-many-iterations-of-rabin-miller-should-i-use-for-cryptographic-safe-primes
    # for justification
    # If number is even, it's a composite number

    if n == 2 or n == 3:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


def pow_mod(B, E, M):
    if E == 0:
        return 1
    elif E == 1:
        return B % M
    else:
        root = pow_mod(B, E // 2, M)
        if E % 2 == 0:
            return (root * root) % M
        else:
            return (root * root * B) % M


def draw_random_prime(min: int = 2**1023, max: int = 2**1024) -> int: # source for RSA prime lengths: https://crypto.stackexchange.com/questions/22971/what-prime-lengths-are-used-for-rsa
    while True:
        p = (2 * random.randint(min, max) + 1) % max
        fermat_test = pow_mod(2, p - 1, p)
        if fermat_test != 1:
            continue
        if miller_rabin(p, 40):
            return p


def modular_inverse(a: int, n: int) -> int:
    t = 0
    newt = 1
    r = n
    newr = a

    while newr != 0:
        quotient = r // newr
        t, newt = newt, t - quotient * newt
        r, newr = newr, r - quotient * newr

    if r > 1:
        raise RuntimeError("a not invertible")
    if t < 0:
        t = t + n

    return t
